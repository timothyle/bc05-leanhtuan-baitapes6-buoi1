const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];

let container=document.getElementById("colorContainer");

loadColorPick=(()=>{
    for(let index=0;index<colorList.length;index++)
    container.innerHTML+=0==index?"<button class='color-button "+colorList[index]+" active'></button>":"<button class='color-button "+colorList[index]+"'></button>"}),
loadColorPick();

let colorPicker=document.getElementsByClassName("color-button");
let house=document.getElementById("house");

for(let index=0;index<colorPicker.length;index++)
colorPicker[index].addEventListener("click",function(){changeColor(colorList[index],index)});

changeColor=((colorList,e)=>{
    for(let index=0;index<colorPicker.length;index++)
    colorPicker[index].classList.remove("active");
    colorPicker[e].classList.add("active"),house.className="house "+ colorList;
});